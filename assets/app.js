import './styles/app.css';

import './bootstrap';

const $ = require('jquery');


    $(document).ready(function () {

        var attributes_catalogs = {
                        'Televisor' : {
                            'label_input' : 'Tamaño de pantalla',
                            'label_select' : 'Tipo de pantalla',                            
                            'options_select' : {
                                'LED':'LED',
                                'LCD':'LCD',
                                'OLED':'OLED'
                            }
                        },
                         'Laptops' : {
                             'label_input' : 'Memoria RAM',
                            'label_select' : 'Procesador',                            
                            'options_select' : {
                                'Intel':'Intel',
                                'AMD':'AMD'
                            }
                        },
                        'Zapatos' : {
                            'label_input' : 'Numero / Tamaño',
                            'label_select' : 'Material',                            
                            'options_select' : {
                                'Piel':'Piel',
                                'Plastico':'Plastico'
                            }
                        }
                    };
      

        $("#products_form_renglon").change(function () {
            var renglon = $('#products_form_renglon').find(":selected").text();
            var select = document.getElementById("select_atributo");
            console.log(attributes_catalogs);

            for (const [key, value] of Object.entries(attributes_catalogs)) {

                  if(key == renglon){

                    document.getElementById('label_select_atributo').innerHTML = value.label_select;
                    document.getElementById("products_form_label_select_atributo_post").value = value.label_select;

                     while (select.options.length > 0) {
                            select.remove(0);
                     }

                    for (var i in value.options_select) {

                        select.options[select.options.length] = new Option(i, i);

                    }
      
                    document.getElementById('label_input_atributo').innerHTML = value.label_input;
                    document.getElementById("products_form_label_input_atributo_post").value = value.label_input;

                }                  
            }
        })

        $("#products_form_renglon").trigger("change");

    });
