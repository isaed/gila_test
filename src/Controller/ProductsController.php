<?php

namespace App\Controller;

use App\Entity\Products;
use App\Entity\Attributes;
use App\Form\ProductsFormType;
use App\Repository\ProductsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductsController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em){

        $this->em = $em;

    }

    #[Route('/products', name: 'app_products')]
    public function index(): Response
    {

        $repository = $this->em->getRepository(Products::class);

        $products = $repository->findAll();

        return $this->render('Products/index.html.twig',[
            "products" => $products
        ]);
    }

    #[Route('/product/{id}', methods: ['GET'], name: 'show_product')]
    public function show($id): Response
    {
        $product_repository = $this->em->getRepository(Products::class);
        $product = $product_repository->find($id);

        $attributes_repository = $this->em->getRepository(Attributes::class);
        $attribute = $attributes_repository->findByIdParent($id);

        switch ($product->getRenglon()) {
            case 'Televisor':
                $costo_ganancia = (35*$product->getCosto())/100 + $product->getCosto();
                break;
            case 'Laptops':
                $costo_ganancia = (40*$product->getCosto())/100 + $product->getCosto();
                break;
            case 'Zapatos':
                $costo_ganancia = (30*$product->getCosto())/100 + $product->getCosto();
                break;
        }


        $product->setCosto($costo_ganancia);

        return $this->render('Products/show.html.twig', [
            'product' => $product,
            'attribute' => $attribute
        ]);
    }

    #[Route('/products/create', name: 'app_products_create')]
    public function create(Request $request): Response
    {

        $product = new Products();

        $form = $this->createForm(ProductsFormType::class, $product);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $newProduct = $form->getData();

            $this->em->persist($newProduct);
            $this->em->flush();
            $this->em->clear();

            $newAttributes = new Attributes();
            $newAttributes->setIdParent($newProduct->getId());
            $newAttributes->setAttributeName($form->get('label_select_atributo_post')->getData());
            $newAttributes->setAttributeValue($request->request->get('select_atributo'));

            $this->em->persist($newAttributes);
            $this->em->flush();
            $this->em->clear();

            $newAttributes->setAttributeName($form->get('label_input_atributo_post')->getData());
            $newAttributes->setAttributeValue($request->request->get('input_atributo'));

            $this->em->persist($newAttributes);
            $this->em->flush();

            return $this->redirectToRoute('app_products');
           
        }

        return $this->render('Products/create.html.twig',[
            "form" => $form->createView()
        ]);
    }

    #[Route('/products/edit/{id}', name: 'app_products_edit')]
    public function edit($id, Request $request): Response
    {

        $product_repository = $this->em->getRepository(Products::class);
        $product = $product_repository->find($id);

        $attributes_repository = $this->em->getRepository(Attributes::class);
        $attribute = $attributes_repository->findByIdParent($id);

        $form = $this->createForm(ProductsFormType::class, $product);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){

            $product->setNombre($form->get('nombre')->getData());
            $product->setSku($form->get('sku')->getData());
            $product->setMarca($form->get('marca')->getData());
            $product->setCosto($form->get('costo')->getData());
            $product->setRenglon($form->get('renglon')->getData());

            $attribute[0]->setAttributeName($form->get('label_select_atributo_post')->getData());
            $attribute[0]->setAttributeValue($request->request->get('select_atributo'));

            $attribute[1]->setAttributeName($form->get('label_input_atributo_post')->getData());
            $attribute[1]->setAttributeValue($request->request->get('input_atributo'));

            $this->em->flush();

            return $this->redirectToRoute('app_products');
           
        }

        return $this->render('Products/edit.html.twig',[
            "form" => $form->createView(),
            'attribute' => $attribute
        ]);
    }

    #[Route('/products/delete/{id}', name: 'app_products_delete')]
    public function delete($id): Response
    {

        $product_repository = $this->em->getRepository(Products::class);
        $product = $product_repository->find($id);

        $attributes_repository = $this->em->getRepository(Attributes::class);
        $attribute = $attributes_repository->findByIdParent($id);

        $this->em->remove($product);
        $this->em->remove($attribute[0]);
        $this->em->remove($attribute[1]);
        $this->em->flush();

        return $this->redirectToRoute('app_products');
    }
  

}
