<?php

namespace App\Entity;

use App\Repository\AttributesRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AttributesRepository::class)]
class Attributes
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'integer')]
    private $id_parent;

    #[ORM\Column(type: 'string', length: 255)]
    private $attribute_name;

    #[ORM\Column(type: 'string', length: 255)]
    private $attribute_value;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdParent(): ?int
    {
        return $this->id_parent;
    }

    public function setIdParent(int $id_parent): self
    {
        $this->id_parent = $id_parent;

        return $this;
    }

    public function getAttributeName(): ?string
    {
        return $this->attribute_name;
    }

    public function setAttributeName(string $attribute_name): self
    {
        $this->attribute_name = $attribute_name;

        return $this;
    }

    public function getAttributeValue(): ?string
    {
        return $this->attribute_value;
    }

    public function setAttributeValue(string $attribute_value): self
    {
        $this->attribute_value = $attribute_value;

        return $this;
    }
}
