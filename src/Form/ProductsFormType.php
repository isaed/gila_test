<?php

namespace App\Form;

use App\Entity\Products;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class ProductsFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nombre', TextType::class, [
                'attr' => array(
                    'class' => 'bg-transparent block border-b-2 w-full h-20 text-6xl outline-none',
                    'placeholder' => 'Enter name...',
                ),
                'label' => false,
                'required' => true
            ])
             ->add('sku', TextType::class, [
                'attr' => array(
                    'class' => 'bg-transparent block border-b-2 w-full h-20 text-6xl outline-none',
                    'placeholder' => 'Enter SKU...',
                ),
                'label' => false,
                'required' => true
            ])
            ->add('marca', TextType::class, [
                'attr' => array(
                    'class' => 'bg-transparent block border-b-2 w-full h-20 text-6xl outline-none',
                    'placeholder' => 'Enter Branch...',
                ),
                'label' => false,
                'required' => true
            ])
            ->add('costo', IntegerType::class, [
                'attr' => array(
                    'class' => 'bg-transparent block mt-10 border-b-2 w-full h-20 text-6xl outline-none',
                    'placeholder' => 'Enter Cost...'
                ),
                'label' => false,
                'required' => true
            ])
            ->add('renglon', ChoiceType::class, array(
                    "attr" => array(
                         'class' => 'bg-transparent block border-b-2 w-full h-20 text-6xl outline-none',
                         'placeholder' => 'Select attribute...',
                    ),
                    'label' => false,
                    'required' => true,
                    'choices'  => array(
                        'Televisor' => 'Televisor',
                        'Laptops' => 'Laptops',
                        'Zapatos' => 'Zapatos'
                    )
             ))
            ->add('label_select_atributo_post', HiddenType::class, [
                'mapped'   => false,
                'data' => 'Tipo de pantalla',
            ])
            ->add('label_input_atributo_post', HiddenType::class, [
                'mapped'   => false,
                'data' => 'Tamaño de pantalla',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Products::class,
        ]);
    }
}
