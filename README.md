## El proyecto contiene imagenes de containers de symfony y mysql para docker

Instanciar los containers:
docker-compose up -d --build

Accesar al container de symfony:
docker exec -it dockersymfony-php-1 bash

Accesar al container de mysql:
docker exec -it dockersymfony-database-1 bash

Migrar las entidades de symfony a la base de datos (creacion de las tablas):
php bin/console make:migration

Iniciar servidor symfony:
symfony server:start -d

URL: http://127.0.0.1:8000/products

